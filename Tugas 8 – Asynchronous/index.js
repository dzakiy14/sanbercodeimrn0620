var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let index = 0;
function RTB(times) {
  if (index < books.length) {
    readBooks(times, books[index], (sisaWaktu) => RTB(sisaWaktu));
  }
  index++;
}

RTB(10000);

