var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let times = 10000;

let i = 0;
function BacaBuku(times, book) {
  readBooksPromise(times, book)
    .then(function (times) {
      i++;
      if (i < books.length) {
        BacaBuku(times, books[i]);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
}

BacaBuku(times, books[i]);

