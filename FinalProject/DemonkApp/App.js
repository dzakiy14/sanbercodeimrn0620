import React from "react";
import Navigasi from "./Tugas/Tugas15/index";
import NavigasiReal from "./Tugas/TugasNavigation/index";

export default function App() {
  return (
    <Navigasi />
    <NavigasiReal />
  );
}
